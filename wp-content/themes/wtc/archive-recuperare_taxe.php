<?php get_header(); ?>

<?php
$args = array(
    'post_type' => 'recuperare_taxe',
    'posts_per_page' => -1,
);
$posts_array = get_posts( $args );
foreach($posts_array as $section){
    $content[$section->post_name] = wpautop($section->post_content);
}
    //print_r ($content);
?>

<div class="container recuperare-taxe main">

    <h1 class="page-title">Recuperare taxe </h1>
    <ul class="nav nav-tabs responsive" id="myTab">
        <li class="active"><a href="#descriere">Descriere servicii</a></li>
        <li><a href="#documente">Documente solicitate</a></li>
        <li><a href="#intrebari">Intrebari frecvente</a></li>
    </ul>

    <div class="tab-content responsive">
        <div class="tab-pane fade in active" id="descriere"><?php echo $content['descriere-servicii']; ?></div>
        <div class="tab-pane fade" id="documente"><?php echo $content['documente-solicitate']; ?></div>
        <div class="tab-pane fade" id="intrebari"><?php echo $content['intrebari-frecvente']; ?></div>

    </div>
</div>
<?php get_footer(); ?>
<script>
    jQuery(document).ready(function($) {
        $('.nav-tabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show');
        })
    });
</script>
