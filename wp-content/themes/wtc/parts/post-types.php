<?php

/*
 * Custom post types
 */

add_action('init', 'cptui_unica');

function cptui_unica() {

    $post_types = array(
        array(
            'slug' => 'Web-Template',
            'name' => 'Web-Template',
            'singular_name' => 'Web-Template',
            'supports' => array('title')
        )
    );

    foreach ($post_types as $post_type) {
        $args = array(
            'labels' => array(
                'name' => __($post_type['name'], THEMENAME),
                'singular_name' => __($post_type['singular_name'], THEMENAME)
            ),
            'public' => true,
            'has_archive' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
            'supports' => $post_type['supports'],
            'menu_icon' => 'dashicons-admin-page'
        );
        if (isset($post_type['supports'])) {
            $args['supports'] = $post_type['supports'];
        }
        if (isset($post_type['taxonomies'])) {
            $args['taxonomies'] = $post_type['taxonomies'];
        }
        if (isset($post_type['show'])) {
            $args['show_ui'] = $post_type['show'];
            $args['show_in_nav_menus'] = $post_type['show'];
            $args['show_in_menu'] = $post_type['show'];
        }
        if (isset($post_type['has_archive'])) {
            $args['has_archive'] = $post_type['has_archive'];
        }
        if (isset($post_type['rewrite'])) {
            $rewrite = array(
                'slug' => $post_type['rewrite'],
                'with_front' => true
            );
            $args['rewrite'] = $rewrite;
        }
        register_post_type($post_type['slug'], $args);
    }
}
