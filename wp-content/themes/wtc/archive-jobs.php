<?php get_header(); ?>

<?php
$args = array(
    'post_type' => 'jobs',
    'posts_per_page' => -1,
);
$posts_array = get_posts( $args );
?>
<div class="container jobs main">
    <h1 class="page-title">Lista job-uri </h1>
    <p class="mesaj-1"><strong>ATENTIE!</strong> Acestea sunt doar o parte din joburile din programul Summer 2018. Pentru lista completa va rugam sa ne contactati la info@workandtravelclub.ro si 0755997788</p>
    <ul class="main-list">
    <?php
    if ( have_posts() ) :
         while ( have_posts() ) : the_post();
    ?>
        <li>
            <?php echo get_the_post_thumbnail('','thumbnail'); ?>
            <?php echo $post->post_title; ?>
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
            <div class="job-content">
                <?php the_content(); ?>
            </div>
        </li>

    <?php
    endwhile;
    endif;
    ?>
    </ul>
</div>
<script>
    jQuery(document).ready(function($) {
        $('ul li').click(function(){
            $('ul li').not(this).removeClass('job-active');
            $(this).toggleClass('job-active');
        });
    });
</script>
<?php get_footer(); ?>
