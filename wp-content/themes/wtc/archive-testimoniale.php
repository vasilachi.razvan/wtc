<!------------>
<!-- HEADER -->
<!------------>

<?php get_header(); ?>

<!------------->
<!-- CONTENT -->
<!------------->

<!--<style>
	.grid {
		background: #DDD;
	}

	/* clear fix */
	.grid:after {
		content: '';
		display: block;
		clear: both;
	}

	/* ---- .grid-item ---- */

	.grid-sizer,
	.grid-item {
		width: 10%;
	}

	.grid-item {
		float: left;
	}

	.grid-item img {
		display: block;
		max-width: 100%;
	}

</style>-->
<!--
<h1>Masonry - imagesLoaded progress</h1>

<div class="grid">
	<div class="grid-sizer"></div>

	<?php
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			$meta = get_post_meta($post->ID);
			$images = unserialize($meta['wiki_file_list'][0]);

			foreach($images as $image){
	?>
	<div class="grid-item">
		<img src="<?php echo $image; ?>" />
	</div>
	<?php }
	endwhile;
	endif;
	?>
</div>


-->



<div class="container main" id="testimonials">
	<h1 class="page-title">Testimoniale</h1>
	<div class="content row">
	<div class="grid-sizer"></div>
	<?php
	
	if ( have_posts() ) :
			while ( have_posts() ) : the_post();


				//print_r ($post);
			?>

				<div class="testimonial col-sm-4 col-xs-12">
					<a href="<?php echo $post->guid;?>">
					<div class="overlay"></div>
					<div class="image">
						
						<?php echo get_the_post_thumbnail('','large'); ?>
					</div>
					<div class="description">
						<div class="title">
							<h6><?php echo $post->post_title; ?></h6>
						</div>
						<div class="the-content">
							<?php //echo $post->post_content; ?>
						</div>
					</div>
					</a>
				</div>
			<?php endwhile;  ?>
		<?php endif; ?>
	</div>
</div>
<!------------>
<!-- FOOTER -->
<!------------>


<?php get_footer(); ?>


<script>
// external js: masonry.pkgd.js, imagesloaded.pkgd.js

// init Isotope
var grid = document.querySelector('.content');

var msnry = new Masonry( grid, {
  itemSelector: '.testimonial',
  columnWidth: '.grid-sizer',
  percentPosition: true,
});

imagesLoaded( grid ).on( 'progress', function() {
  // layout Masonry after each image loads
  msnry.layout();
});


var $grid = $('.grid').masonry({
	itemSelector: '.grid-item',
	percentPosition: true,
	columnWidth: '.grid-sizer'
});
// layout Isotope after each image loads
$grid.imagesLoaded().progress( function() {
	$grid.masonry();
});

</script>