<?php
/**
 * The template for displaying any single post.
 *
 */

get_header(); // This fxn gets the header.php file and renders it ?>
	<div class="container main">

			<?php if ( have_posts() ) :
			// Do we have any posts in the databse that match our query?
			?>

				<?php while ( have_posts() ) : the_post(); 
				// If we have a post to show, start a loop that will display it
				
				$content = apply_filters( 'the_content', get_the_content() );
				$meta 	 = get_post_meta($post->ID);
				$gallery = unserialize($meta['wiki_file_list'][0]);
//				print_r ($gallery);
				?>
				<h1 class="page-title"><?php echo $post->post_title; ?><br>
				 
				<span>(<?php echo $post->post_type; ?>)</span></h1>


				<!------------------>
				<!-- POST CONTENT -->
				<!------------------>

				<div class="post-content col-sm-12">
						<div class="swiper-container">
							<div class="swiper-wrapper">								
								<?php								if($post->post_type=='testimoniale'){								foreach($gallery as $image) { ?>
									<div class="swiper-slide"><img src="<?php echo $image; ?>"></div>
								<?php }} ?>
							</div>
							<!-- Add Pagination -->
							<div class="swiper-pagination"></div>
							<!-- Add Arrows -->
							<div class="swiper-button-next"></div>
							<div class="swiper-button-prev"></div>
						</div>
					<div class="description"><?php echo $content; ?></div>
				</div>
			<?php endwhile; // OK, let's stop the post loop once we've displayed it ?>
			<?php endif; // OK, I think that takes care of both scenarios (having a post or not having a post to show) ?>
	</div>

<!-- SLIDER -->

<script>
	jQuery(document).ready(function($) {
		var swiper = new Swiper('.swiper-container', {
			pagination: '.swiper-pagination',
			paginationClickable: true,
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			spaceBetween: 100
		});
	});
</script>

<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
