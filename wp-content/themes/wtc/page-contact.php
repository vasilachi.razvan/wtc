<?php
/** Template name: Contact */

get_header(); // This fxn gets the header.php file and renders it ?>
    <div class="container main">
        <h1 class="page-title">Contact</h1>
        <?php echo do_shortcode('[contact-form-7 id="530" title="Page Contact form"]'); ?>
        <div class="contactbreak"></div>
        <div class="col-sm-6 col-xs-12">
            <h3>Bucuresti</h3>
            <div class="details">
                <i class="fa fa-location-arrow" aria-hidden="true"></i> <strong>Adress:</strong> Bulevarul Regina Maria Elisabeta
                nr.7-9, et.3, camere 4-10<br>
                <i class="fa fa-phone" aria-hidden="true"></i><strong> Tel / Fax:</strong> 021.335.08.12 |
                <strong>Mobil:</strong> 0755.99.77.88 / 0725.97.76.50<br>
                <i class="fa fa-envelope" aria-hidden="true"></i><strong>Email:</strong> info@workandtravelclub.ro<br>
                For business partners use office@workandtravelclub.ro
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d598.9113708869526!2d26.100444899615272!3d44.43483082361993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3af52773bca1b182!2sWork+and+Travel+Club!5e0!3m2!1sen!2sro!4v1479810570362" width="500" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div class="col-sm-6 col-xs-12">
            <h3>Cluj-Napoca</h3>
            <div class="details">
                <i class="fa fa-location-arrow" aria-hidden="true"></i> <strong>Adress:</strong> Bvd. Eroilor, nr. 7, ap. 4<br>
                <i class="fa fa-phone" aria-hidden="true"></i><strong> Mobil:</strong> 0755.22.33.11<br>
                <i class="fa fa-envelope" aria-hidden="true"></i><strong>Email:</strong> cluj@workandtravelclub.ro<br>
                For business partners use office@workandtravelclub.ro
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d966.1753452987829!2d23.59166560918144!3d46.7696948599526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47490c279387a92d%3A0x838f551484037777!2sWork+and+Travel+Club!5e0!3m2!1sen!2sus!4v1479811105177" width="500" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>