<?php
/**
 * Template name: Home
 *
 */
get_header(); // This fxn gets the header.php file and renders it ?>

	<!----------->
	<!-- JOBS --->
	<!----------->

	<div class="container jobs home-section">
		<h1 class="title">Lista joburi USA</h1>
		<div class="row">
			<div class="col-sm-4 col-xs-12 job">
				<div class="image">
					<img src="/wp-content/uploads/2017/01/home-job-1-test.jpg">
				</div>
				<div class="job-title">
					<h4>Ice cream shop - Laguna Beach, CA</h4>
				</div>
				<div class="job-description">
					Number of students: 6 <i class="fa fa-long-arrow-right" aria-hidden="true"></i> 4 <i class="fa fa-male" aria-hidden="true"></i>, 2 <i class="fa fa-female" aria-hidden="true"></i><br>
					Positions: ice cream salesperson - <i class="fa fa-female" aria-hidden="true"></i> / <i class="fa fa-male" aria-hidden="true"></i><br>
					Estimated h/week: ~ 40$<br>
					Overtime: yes
				</div>
				<div class="job-details">
					<div class="salary">
						Salary: $10/h
					</div>
					<div class="link">
						<a href="lista-joburi/ice-cream-shop-laguna-beach-ca/">Citeste mai mult</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="col-sm-4 col-xs-12 job">
				<div class="image">
					<img src="/wp-content/uploads/2017/01/home-job-2-test.jpg">
				</div>
				<div class="job-title">
					<h4>New York, NY - Restaurant</h4>
				</div>
				<div class="job-description">
					Number of students: 4 <br>
					Positions: Barista, Waiter<br>
					Estimated h/week: ~ 40$ <br>
					Overtime: yes
				</div>
				<div class="job-details">
					<div class="salary">
						Salary: $5/h + tips
					</div>
					<div class="link">
						<a href="lista-joburi/new-york-ny-restaurant/">Citeste mai mult</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="col-sm-4 col-xs-12 job">
				<div class="image">
					<img src="/wp-content/uploads/2017/01/home-job-3-test.jpg">
					<div class="job-title">
						<h4>Virginia Beach, Virginia, (VA)</h4>
					</div>
					<div class="job-description">
						Number of students: 7 <i class="fa fa-long-arrow-right" aria-hidden="true"></i> 1 <i class="fa fa-male" aria-hidden="true"></i>, 6 <i class="fa fa-female" aria-hidden="true"></i><br>
						Positions: Waitress 6 - <i class="fa fa-female" aria-hidden="true"></i>, cooks - 1 <i class="fa fa-male" aria-hidden="true"></i><br>
						Estimated h/week: ~ 40$<br>
						Overtime: could give extra hours in other business
					</div>
					<div class="job-details">
						<div class="salary">
							Salary: $10/h + tips
						</div>
						<div class="link">
							<a href="lista-joburi/virginia-beach-va-restaurant/">Citeste mai mult</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--------------->
    <!---- FORM ----->
    <!--------------->
    <div class="find-more">

        <div class="container">
            <h1>Afla mai multe! Un consultant WTC te va contacta in cel mai scurt timp posibil.</h1>
            <?php echo do_shortcode('[contact-form-7 id="515" title="Home-form"]'); ?>
        </div>
    </div>
	
	<!--------------->
	<!-- PROGRAME --->
	<!--------------->

	<div class="programs home-section">
		<div class="container">
			<h1 class="title">Programe</h1>
            <div class="text">
                Cu siguranta, experienta de viata cu care te vei alege indiferent ca optezi pentru unul dintre Programele <br>WORK AND TRAVEL SUA sau EUROPA te va ajuta in formarea ta in viitor.
            </div>
            <iframe width="640" height="360" src="https://www.youtube.com/embed/EXbwZ8h3jjc" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>

    <!------------------>
    <!-- TESTIMONIALE -->
    <!------------------>

    <div class="container h-testimonials home-section">
        <h1 class="title">Testimoniale</h1>
        <div class="col-sm-3 col-xs-12 h-testimonial">
            <div class="htwrapper">
                <div class="image">
                    <img src="/wp-content/uploads/2017/02/home-testimonial-1-optim.jpg">
                </div>
                <div class="name">
                    Loredana<br><span>New York</span>
                </div>
                <div class="description">
                     America, America, America...trei veri magice de care m-am bucurat si in am crescut din punct de vedere ...
                </div>
                <div class="link">
                    <a href="/testimoniale/loredana-new-york-city-new-york/">
                        CITESTE TOT
                    </a>
                </div>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12 h-testimonial">
            <div class="htwrapper">
                <div class="image">
                    <img src="/wp-content/uploads/2017/02/home-testimonial-2-optim.jpg">
                </div>
                <div class="name">
                    Razvan<br><span>Jackson Hole</span>
                </div>
                <div class="description">
                    Vara aceasta mi-am petrecut-o in Jackson Hole, Wyoming, un orasel micut, de vreo 10.000 de oameni ...
                </div>
                <div class="link">
                    <a href="/testimoniale/razvan-jackson-hole-wy">
                        CITESTE TOT
                    </a>
                </div>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12 h-testimonial">
            <div class="htwrapper">
                <div class="image">
                    <img src="/wp-content/uploads/2017/02/home-testimonial-3-optim.jpg">
                </div>
                <div class="name">
                    Laura<br><span>Sister Bay</span>
                </div>
                <div class="description">
                    Infime sunt cuvintele care pot descrie pe deplin aventura trăită la mii de km de casa. De fiecare ...
                </div>
                <div class="link">
                    <a href="/testimoniale/laura-sister-bay-wisconsin">
                        CITESTE TOT
                    </a>
                </div>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12 h-testimonial">
            <div class="htwrapper">
                <div class="image">
                    <img src="/wp-content/uploads/2017/02/home-testimonial-4-optim.jpg">
                </div>
                <div class="name">
                    Victoria<br><span>Lake Placid</span>
                </div>
                <div class="description">
                    Ultimele trei veri petrecute in state au fost cele mai frumoase din viata! Toate trei au fost pline de ...
                </div>
                <div class="link">
                    <a href="/testimoniale/victoria-lake-placid-new-york">
                        CITESTE TOT
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!------------->
    <!-- CITESTE -->
    <!------------->

<!--    <div class="home-citeste">-->
<!--        <div class="container home-section">-->
<!--            <div class="text">CU NOI POTI SA ACHITI COSTUL PROGRAMULUI CAND TE INTORCI</div>-->
<!--            <a href="">CITESTE MAI MULTE ></a>-->
<!--        </div>-->
<!--    </div>-->

    
    <!--------------->
    <!-- PROCEDURI -->
    <!--------------->

    <div class="container home-section proceduri">
        <h1 class="title">Proceduri si eligibilitate</h1>
        <div class="col-sm-6 col-xs-12 procedura">
            <a href="/programe-sua/">
                <i class="fa fa-star" aria-hidden="true"></i>
                <div class="text">
                    <h5>Despre program work and travel</h5>
                    Din experienta noastra dar si a celorlalti studenti care au ales programul work and travel club pentru a trai visul american, impartasim si voua motivele ce ...
                </div>
            </a> 
        </div>
        <div class="col-sm-6 col-xs-12 procedura">
            <a href="/programe-sua/">
                <i class="fa fa-trophy" aria-hidden="true"></i>
                <div class="text">
                    <h5>Costuri program work and travel USA</h5>
                    Beneficiaza de oferta noastra Early Bird si inscrie-te in programul Work and Travel USA cu numai 199 lei pentru a prinde cele mai bune oferte de munca!
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-xs-12 procedura">
            <a href="/programe-sua/">
                <i class="fa fa-magic" aria-hidden="true"></i>
                <div class="text">
                    <h5>Conditii de participare</h5>
                    Afla ca de anul acesta conditiile pentru a putea participa la Programul Work and Travel USA sunt foarte permisive. Nu conteaza daca universitatea este privata ...
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-xs-12 procedura">
            <a href="/programe-sua/">
                <i class="fa fa-cloud-download" aria-hidden="true"></i>
                <div class="text">
                    <h5>Dosar acte program work and travel</h5>
                    Pentru a avea drept legal de munca si calatorie in State ai nevoie de viza J1 acordata de ambasada SUA din Bucuresti, in urma unui interviu cu intrebari standard de ...
                </div>
            </a>
        </div>
    </div>

    <!---------------->
    <!---- BANNER  --->
    <!---------------->

    <div class="h-banner">
        <img src="/wp-content/uploads/2016/11/banner-home.png">
    </div>

    <!--------------->
    <!-- PARTENERI -->
    <!--------------->

    <div class="parteneri">
        <div class="container home-section">
            <h1 class="title">Parteneri Principali Work And Travel Club</h1>
            <a href="">
                <img src="/wp-content/uploads/2016/11/parteneri.png">
            </a>
        </div>

    </div>


<?php get_footer(); // This fxn gets the footer.php file and renders it ?>