<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>
	<?php bloginfo('name');  ?> | 
	<?php is_front_page() ? bloginfo('description') : wp_title(''); // if we're on the home page, show the description, from the site's settings - otherwise, show the title of the post or page ?>
</title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>
	<!-- Hotjar Tracking Code for http://www.workandtravelclub.ro -->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:541430,hjsv:5};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
	</script>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68195170-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>

<header id="masthead" class="site-header">
	<div class="top">
		<div class="container">
			<ul class="telephone-numbers pull-right">
				<li>Telekom: 021.335.08.12</li>
				<li>Orange: 0755.997.788</li>
				<li>Vodafone: 0725.97.76.50</li>
			</ul>

			<div class="logo">
				<a href="<?php echo site_url(); ?>">
					<img src="/wp-content/uploads/2016/11/logo-top-2-2.png">
				</a>
			</div>
		</div>
	</div>
	
	<div class="top-menu">

		<div class="container">
			<ul class="colorbar">
				<li></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
			</ul>
			<div id="nav-icon4" class="mobile">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<nav class="site-navigation main-navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) );  ?>
			</nav>
		</div>
	</div>
</header>

<?php 


if(is_front_page()) { ?>
	<div class="fullwidthimage">
		<img src="http://workandtravelclub.ro//wp-content/themes/wtc/images/background-home-mobile-3.jpg" class="img-responsive">
	</div>
<?php }elseif(get_the_ID()=='458'){ ?>
	<!--<img src="/wp-content/uploads/2016/11/testimonial-background-1.png" class="img-responsive">-->
<?php } ?>
