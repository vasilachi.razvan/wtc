<?php get_header();
    /** Template name: Descriere Program */
?>

<?php
    $args = array(
        'post_type' => 'programe_sua',
        'posts_per_page' => -1,
    );
    $posts_array = get_posts( $args );
    foreach($posts_array as $section){
        $content[$section->post_name] = wpautop($section->post_content);
    }

?>

    <div class="container programe main">

        <h1 class="page-title">Work And Travel USA </h1>
        <ul class="nav nav-tabs responsive" id="myTab">
            <li class="active"><a href="#descriere">Descriere program</a></li>
            <li><a href="#conditii">Conditii eligibilitate</a></li>
            <li><a href="#costuri">Costuri</a></li>
            <li><a href="#acte">Dosar acte</a></li>
        </ul>

        <div class="tab-content responsive">
            <div class="tab-pane fade in active" id="descriere"><?php echo $content['descriere-program']; ?></div>
            <div class="tab-pane fade" id="conditii"><?php echo $content['conditii-eligibilitate']; ?></div>
            <div class="tab-pane fade" id="costuri"><?php echo $content['costuri']; ?></div>
            <div class="tab-pane fade" id="acte"><?php echo $content['dosar-acte']; ?></div>
        </div>
    </div>
<?php get_footer(); ?>
<script>
    jQuery(document).ready(function($) {
        $('.nav-tabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show');
        })
    });
</script>
