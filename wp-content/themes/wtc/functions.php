<?php
	/*-----------------------------------------------------------------------------------*/
	/* This file will be referenced every time a template/page loads on your Wordpress site
	/* This is the place to define custom fxns and specialty code
	/*-----------------------------------------------------------------------------------*/

// Define the version so we can easily replace it throughout the theme
add_theme_support('post-thumbnails');

register_nav_menus( 
	array(
		'primary'	=>	__( 'Primary Menu', 'naked' ), // Register the Primary menu
		// Copy and paste the line above right here if you want to make another menu, 
		// just change the 'primary' to another name
	)
);



/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/
function sidebar() {

    register_sidebar( array(
        'name'          => 'Sidebar',
        'id'            => 'sidebar',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'sidebar' );



function resources()  { 

	// get the theme directory style.css and link to it in the header
	
	wp_enqueue_style('fontawesome.css', get_stylesheet_directory_uri() . '/styles/font-awesome.min.css');
    wp_enqueue_style('swiper.css', get_stylesheet_directory_uri() . '/styles/swiper.min.css');
	wp_enqueue_style('bootstrap.css', get_stylesheet_directory_uri() . '/styles/bootstrap.min.css');
	wp_enqueue_style('style.css', get_stylesheet_directory_uri() . '/style.css');
	wp_enqueue_style('custom.css', get_stylesheet_directory_uri() . '/styles/custom.css');
	
	
	// add theme scripts
	
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-3.1.1.min.js', array(), false, true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), false, true );
    wp_enqueue_script( 'swiper', get_template_directory_uri() . '/js/swiper.min.js', array(), false, true );
	wp_enqueue_script( 'masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array(), false, true );
	wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.js', array(), false, true );

  
}
add_action( 'wp_enqueue_scripts', 'resources' ); // Register this fxn and allow Wordpress to call it automatcally in the header


register_post_type( 'programe_sua',
    array(
        'labels'                => array(
            'name'              => __( 'Programe SUA' ),
            'singular_name'     => __( 'Programe SUA' )
            ),
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'supports'              => array( 'title', 'editor', "thumbnail", "author", "custom-fields", "comments"),
        'rewrite'               => array( 'slug' => 'programe-sua', 'with_front' => true ),
        'has_archive'           => true,
		'menu_position'			=> 6
    )
);

register_post_type( 'programe_europa',
    array(
        'labels'                => array(
            'name'              => __( 'Programe Europa' ),
            'singular_name'     => __( 'Programe Europa' )
            ),
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'supports'              => array( 'title', 'editor', "thumbnail", "author", "custom-fields", "comments"),
        'rewrite'               => array( 'slug' => 'programe-europa', 'with_front' => true ),
        'has_archive'           => true,
		'menu_position'			=> 6
    )
);

register_post_type( 'recuperare_taxe',
    array(
        'labels'                => array(
            'name'              => __( 'Recuperare Taxe' ),
            'singular_name'     => __( 'Recuperare Taxe' )
            ),
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'supports'              => array( 'title', 'editor', "thumbnail", "author", "custom-fields", "comments"),
        'rewrite'               => array( 'slug' => 'recuperare-taxe', 'with_front' => true ),
        'has_archive'           => true,
		'menu_position'			=> 6
    )
);

register_post_type( 'jobs',
    array(
        'labels'                => array(
            'name'              => __( 'Lista Job-uri' ),
            'singular_name'     => __( 'Lista Job-uri' )
            ),
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'supports'              => array( 'title', 'editor', "thumbnail", "author", "custom-fields", "comments"),
        'rewrite'               => array( 'slug' => 'lista-joburi', 'with_front' => true ),
        'has_archive'           => true,
		'menu_position'			=> 6
    )
);

register_post_type( 'news',
    array(
        'labels'                => array(
            'name'              => __( 'Stiri' ),
            'singular_name'     => __( 'Stiri' )
            ),
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'supports'              => array( 'title', 'editor', "thumbnail", "author", "custom-fields", "comments"),
        'rewrite'               => array( 'slug' => 'lista-stiri', 'with_front' => true ),
        'has_archive'           => true,
		'menu_position'			=> 6
    )
);

register_post_type( 'social',
    array(
        'labels'                => array(
            'name'              => __( 'Social Buttons' ),
            'singular_name'     => __( 'Social Buttons' )
            ),
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'supports'              => array( 'title', 'editor', "thumbnail", "author", "custom-fields", "comments"),
        'rewrite'               => array( 'slug' => 'social-buttons', 'with_front' => true ),
        'has_archive'           => true,
		'menu_position'			=> 6
    )
);

register_post_type( 'testimoniale',
    array(
        'labels'                => array(
            'name'              => __( 'Testimoniale' ),
            'singular_name'     => __( 'Testimoniale' )
            ),
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'supports'              => array( 'title', 'editor', "thumbnail", "author", "custom-fields", "comments", "excerpt"),
        'rewrite'               => array( 'slug' => 'testimoniale', 'with_front' => true ),
        'has_archive'           => true,
		'menu_position'			=> 6
    )
);


add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_sample_metaboxes() {

    $prefix = '_cmb2_';

    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Test Metabox', 'cmb2' ),
        'object_types'  => array( 'testimoniale', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

   $cmb->add_field( array(
		'name' => 'Files',
		'desc' => '',
		'id'   => 'wiki_file_list',
		'type' => 'file_list',
		'preview_size' => array( 150, 150 ), // Default: array( 50, 50 )
		// Optional, override default text strings
		
	) );
	

}