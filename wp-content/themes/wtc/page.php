<?php
/**
 * The template for displaying any single page.
 *
 */

get_header(); // This fxn gets the header.php file and renders it ?>
	<div class="container main">
		<h1 class="page-title"><?php echo get_the_title(); ?></h1>
		<?php if ( have_posts() ) :
			 while ( have_posts() ) : the_post();
					 the_content();
		 endwhile;

		 endif; ?>
	</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>