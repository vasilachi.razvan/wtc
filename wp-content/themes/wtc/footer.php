<div class="container" id="more-partners">
        <a href="https://www.brd.ro/persoane-fizice/credite/credite-personale/creditul-worktravel" target="_blank"><img src="/wp-content/uploads/2016/09/BRD.jpg"></a>
        <a href="http://www.culturalexchangenetwork.org/Default.aspx" target="_blank"><img src="/wp-content/uploads/2016/09/CENET.jpg"></a>
        <a href="http://www.dynamicglobalexchange.com/" target="_blank"><img src="/wp-content/uploads/2017/02/DYNAMIC-GLOBAL-EXCHANGE1-optim.jpg"></a>
        <a href="http://www.intraxworktravel.com/" target="_blank"><img src="/wp-content/uploads/2016/09/INTRAX.png"></a>
        <a href="https://www.wysetc.org/" target="_blank"><img src="/wp-content/uploads/2017/02/Wyse-Logo-optim.jpg"></a>
        <a href="http://www.rttax.com/" target="_blank"><img src="/wp-content/uploads/2016/09/RTTAX1.gif"></a>
        <a href="http://romania.usembassy.gov/visas/summer_work.html" target="_blank"><img src="/wp-content/uploads/2016/09/USA-EMBASSY.png"></a>
        <a href="http://www.janus-international.com/" target="_blank"><img src="/wp-content/uploads/2016/09/Janus1.png"></a>
</div>
<footer>
	<div class="container no-padding">
        <div class="col-sm-3 colour"></div>
        <div class="col-sm-3 colour"></div>
        <div class="col-sm-3 colour"></div>
        <div class="col-sm-3 colour"></div>

        <div class="col-sm-3 facebook-box">
            <h3>Follow us</h3>
            <iframe src="https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FWork-and-Travel-Club%2F205234369490921&amp;width=250&amp;height=230&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; min-height:230px;" allowtransparency="true"></iframe>
        </div>

        <div class="col-sm-6 f-testimonials">
            <h3>Testimonials</h3>
			
            <div class="f-test">
				<a href="/testimoniale/dana-wisconsin-dells-wisconsi/">
                <img src="/wp-content/uploads/2017/02/ghelmez.jpg">
                <div class="text">
                    Vara asta a fost una plina cu experiente noi, locuri noi, prieteni noi din toata lumea, job-uri noi.. Desi la inceput a fost fooarte greu, in principiu din cauza distantei foarte mari de la cazare la locul de munca, treptat m-am adaptat si Wisconsin a devenit "acasa".. toata munca si-a primit rasplata o data cu partea de travel, saptamana petrecuta in ...
                </div>
				</a>
            </div>
            <div class="f-test">
				<a href="/testimoniale/laura-sister-bay-wisconsin/">
                <img src="/wp-content/uploads/2016/10/DSC_5236web-300x199.jpg">
                <div class="text">
                    Infime sunt cuvintele care pot descrie pe deplin aventura trăită la mii de km de casă. De fiecare dată când mă gândesc la experiența din America, zâmbesc. Sună a clișeu, dar a fost de departe cea mai frumoasă vară de care m-am bucurat până acum. Partea cea mai bună este că nici măcar nu pot să separ săptămânile de călătorit ...
                </div>
				</a>
            </div>
        </div>

        <div class="col-sm-3 get-in-touch">
            <h3>Get in touch</h3>
            <i class="fa fa-location-arrow" aria-hidden="true"></i><span>Adress:</span> Bulevarul Regina Maria Elisabeta<br> nr.7-9, et.3, camere 4-10<br><br><br>
            <div class="clearboth"></div>
            <i class="fa fa-phone" aria-hidden="true"></i><span>Tel / Fax:</span> 021.335.08.12<br>
            <span>Mobil:</span> 0755.99.77.88 / 0725.97.76.50<br><br><br>
            <div class="clearboth"></div>
            <i class="fa fa-envelope" aria-hidden="true"></i><span>Email:</span> info@workandtravelclub.ro
        </div>
		
    </div>
</footer>
<!--<a href="" id="return-to-top" style="display:block;"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>-->

<?php wp_footer(); ?>
<script>
    jQuery(document).ready(function($) {
        $(window).scroll(function () {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });

        $('#return-to-top').click(function () {      // When arrow is clicked
            $('body,html').animate({
                scrollTop: 0                       // Scroll to top of body
            }, 'slow');
        });


        $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
            $(this).toggleClass('open');
            $('.site-navigation').toggle('2000');
        });

        $('.nav-tabs li').click(function(){
            $('.tab-content').addClass('tab-top-padding');
            $('html, body').animate({
                scrollTop: $("#myTab").offset().top
            }, 400);
        });

        });
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/560e46dc20036ca862a7486d/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->

</body>
</body>
</html>
